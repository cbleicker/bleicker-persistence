<?php
namespace Bleicker\Persistence;

use Doctrine\ORM\EntityManager as EntityManagerOrigin;

/**
 * Class EntityManager
 */
class EntityManager extends EntityManagerOrigin implements EntityManagerInterface {

}
